const router = require('express').Router();
const apiController = require('../controllers/apiController');
const bodyparser = require('body-parser');

router.use(bodyparser.json());
router.use(bodyparser.urlencoded({ extended: true }));

router
.get('/characters/votes' , apiController.charactersVotes)
.get('/characters/:id/votes' , apiController.charactersByIdVotes)
.get('/user/:id/votes' , apiController.userByIdVotes)
.get('/me/votes' , apiController.meVotes)

module.exports = router;