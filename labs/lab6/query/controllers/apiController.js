const config = require("../config");
const mongoose = require('mongoose');
const jwt = require("jsonwebtoken");
const Character = require("../models/character");
const Vote = require("../models/vote");
const User = require("../models/user");

const connectOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
};
const dbUrl = config.url;

function checkToken(token)
{
    return jwt.verify(token , config.jwtSecret , (err , verifiedJwt) => {
        if(err)
        {
            return 401;
        }
        else
        {
            return true;
        }
    });
}

function unpackToken(token)
{
    return jwt.decode(token);
}

async function getUser(user) {
    const model = mongoose.model("users", User);
    return mongoose.connect(dbUrl, connectOptions).then(() => { return model.find({ "login": user }) }).catch((error) => { return 498 });
}

async function getCharacters() {
    const model = mongoose.model("characters", Character);
    return mongoose.connect(dbUrl, connectOptions).then(() => { return model.find({}) }).catch((error) => { return 499 });
}

async function getVotesByCharacterId(characterId) {
    const model = mongoose.model("votes", Vote);
    return mongoose.connect(dbUrl, connectOptions).then(() => { return model.find({ "character_id": characterId }).sort({ "time": "desc" }) }).catch((error) => { return 496 });
}

async function createQueryEntities() {
    const charactersVotes = new Array();
    const characters = await getCharacters();
    for (i = 0; i < characters.length; i++) {
        const result = {
            character_id: characters[i]._id,
            name: characters[i].name,
            votes: null
        }
        const votes = await getVotesByCharacterId(characters[i]._id);
        if (votes == 500 || votes.length == 0) {
            charactersVotes.push(result);
            continue;
        } else {
            const result_votes = new Array();
            for (j = 0; j < votes.length; j++) {
                const user = await getUser(votes[j].user);
                const voteUser = {
                    user_id: user[0]._id,
                    login: user[0].login
                }
                const vote = {
                    time: votes[j].time,
                    user: voteUser
                }
                result_votes.push(vote);
            }
            result.votes = result_votes;
        }
        charactersVotes.push(result);
    }
    return charactersVotes;
}

module.exports = {
    async charactersVotes(req, res) {
        if (req.query.page == null || req.query.size == null) {
            res.status(400).send("Bad request");
            return;
        }
        const page = parseInt(req.query.page);
        const amount = parseInt(req.query.size);
        const offset = (page - 1) * amount;
        const charactersVotes = await createQueryEntities();
        const new_charactersVotes = charactersVotes.slice(offset, offset + amount);
        res.status(200).json(new_charactersVotes);
    },

    async charactersByIdVotes(req, res) {
        if (req.params.id == null) {
            res.status(400).send("Bad request");
            return;
        }
        const characterId = req.params.id;
        const charactersVotes = await createQueryEntities();
        for (i = 0; i < charactersVotes.length; i++) {
            if (charactersVotes[i].character_id == characterId) {
                res.status(200).json(charactersVotes[i]);
                return;
            }
        }
        res.status(404).send("Not found");
    },

    async userByIdVotes(req, res) {
        if (req.query.page == null || req.query.size == null) {
            res.status(400).send("Bad request");
            return;
        }
        if (req.params.id == null) {
            res.status(400).send("Bad request");
            return;
        }
        const userId = req.params.id;
        const page = parseInt(req.query.page);
        const amount = parseInt(req.query.size);
        const offset = (page - 1) * amount;
        const charactersVotes = await createQueryEntities();
        const result = new Array();
        for (i = 0; i < charactersVotes.length; i++) {
            if (charactersVotes[i].votes == null) {
                continue;
            }
            for (j = 0; j < charactersVotes[i].votes.length; j++) {
                if (charactersVotes[i].votes[j].user.user_id == userId) {
                    result.push(charactersVotes[i]);
                }
            }
        }
        const new_result = result.slice(offset, offset + amount);
        res.status(200).json(new_result);;
    },

    async meVotes(req , res)
    {
        if (req.query.page == null || req.query.size == null) {
            res.status(400).send("Bad request");
            return;
        }
        if(req.query == null)
        {
            res.status(400).send("Empty or null query");
            return;
        }
        if(req.query.token == null)
        {
            res.status(400).send("No login");
            return;
        }
        const token = req.query.token;
        const verify = checkToken(token);
        if(verify == 401)
        {
            res.status(401).send("No login");
            return;
        }
        const user = unpackToken(token);
        const fullUser = await getUser(user.login);
        console.log(fullUser);
        const page = parseInt(req.query.page);
        const amount = parseInt(req.query.size);
        const offset = (page - 1) * amount;
        const charactersVotes = await createQueryEntities();
        const result = new Array();
        for (i = 0; i < charactersVotes.length; i++) {
            if (charactersVotes[i].votes == null) {
                continue;
            }
            for (j = 0; j < charactersVotes[i].votes.length; j++) {
                if (charactersVotes[i].votes[j].user.user_id.toString() == fullUser[0]._id.toString()) {
                    result.push(charactersVotes[i]);
                }
            }
        }
        const new_result = result.slice(offset, offset + amount);
        res.status(200).json(new_result);;
    }
}