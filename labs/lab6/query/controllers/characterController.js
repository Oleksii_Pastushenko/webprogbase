const { json } = require('body-parser');
const config = require("../config");
const mongoose = require('mongoose');
const { ObjectID } = require("bson");
const jwt = require("jsonwebtoken");
const Character = require("../models/character");

const connectOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
};
const dbUrl = config.url;
const model = mongoose.model("characters" , Character);

async function addCharacter(character)
{
    return mongoose.connect(dbUrl, connectOptions).then(() => model.insertMany(new model(character))).catch((error) => { res.status(500); return; });  
}

async function updateCharacter(character)
{
    return mongoose.connect(dbUrl , connectOptions).then(() => {return model.find({})}).then(() => {return model.updateOne({"_id": character._id} , character)}).catch((error)=> {return 500;})
}

async function deleteCharacter(character)
{
    return mongoose.connect(dbUrl , connectOptions).then(() => model.find({})).then(() => {return model.remove({_id: character._id})}).catch((error)=> {return 500});
}

module.exports = {
    async characterCRUD(message)
    {
        switch(message.type) {
            case 'add':
                delete message.type;
                await addCharacter(message);
                break;
            case 'update':
                delete message.type;
                await updateCharacter(message);
                break; 
            case 'delete':
                delete message.type;
                await deleteCharacter(message);
                break; 
            default:
                console.log("Error with character messages"); 
                break;
        }
    }
}
