const { Schema } = require("mongoose");
const { ObjectID } = require('mongodb');

const Vote = new Schema({
    _id: ObjectID,
    time: Date,
    user: String,
    character_id: ObjectID
})

module.exports = Vote;