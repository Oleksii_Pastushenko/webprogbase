const router = require('express').Router();
const apiController = require('../controllers/apiController');
const bodyparser = require('body-parser');
const { get } = require('http');

router.use(bodyparser.json());
router.use(bodyparser.urlencoded({ extended: true }));

router
.get("/characters/:characterId" , apiController.getCharacterById)
.get("/characters" , apiController.getCharacters)
.post("/characters" , apiController.addCharacter)
.put("/characters" , apiController.updateCharacter)
.post("/characters" , apiController.deleteCharacter)
module.exports = router;