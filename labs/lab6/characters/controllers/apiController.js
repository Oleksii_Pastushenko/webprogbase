const Character = require("../models/character");
const config = require("../config");
const { json } = require('body-parser');
const mongoose = require('mongoose');
const { ObjectID } = require("bson");
const jwt = require("jsonwebtoken");
const amqp = require('amqplib');

const connectOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
};
const dbUrl = config.url;
const model = mongoose.model("characters", Character);

function checkToken(token) {
    return jwt.verify(token, config.jwtSecret, (err, verifiedJwt) => {
        if (err) {
            return 401;
        }
        else {
            return true;
        }
    });
}

function unpackToken(token) {
    return jwt.decode(token);
}

async function getCharacterById(characterId) {
    return mongoose.connect(dbUrl, connectOptions).then(() => { return model.find({ "_id": characterId }) }).catch((error) => { return 500 });
}

async function getAllCharacters() {
    return mongoose.connect(dbUrl, connectOptions).then(() => { return model.find({}) }).catch((error) => { return 500 });
}

async function updateCharacterById(character)
{
    return mongoose.connect(dbUrl , connectOptions).then(() => {return model.find({})}).then(() => {return model.updateOne({"_id": character._id} , character)}).catch((error)=> {return 500;})
}

module.exports = {
    async getCharacterById(req, res) {
        const characterId = req.params.characterId;
        const character = await getCharacterById(characterId);
        if (character == 500 || character.length == 0) {
            res.status(500).send("Error");
        }
        res.status(200).json(character[0]);
    },

    async getCharacters(req, res) {
        if (req.query.page == null || req.query.size == null) {
            res.status(400).send("Bad request");
        } else {
            const page = parseInt(req.query.page);
            const amount = parseInt(req.query.size);
            const offset = (page - 1) * amount;
            const characters = await getAllCharacters();
            const new_characters = characters.slice(offset, offset + amount);
            res.status(200).json(new_characters);
        }
    },

    async addCharacter(req, res) {
        if (req.body == null) {
            console.log(69);
            res.status(400).send("Bad request");
            return;
        }
        if (req.body.name == null || req.body.rank == null || req.body.age == null) {
            console.log(74);
            res.status(400).send("Empty or null input");
            return;
        }
        const token = req.body.token;
        if (token == null && token == undefined) {
            console.log(80);
            res.status(401).send("No login");
            return;
        }
        const check = checkToken(token);
        if (check == 401) {
            console.log(86);
            res.status(401).send("No login");
            return;
        }
        const author = unpackToken(token);
        const character = {
            _id: new ObjectID(),
            name: req.body.name,
            rank: req.body.rank,
            age: req.body.age,
            author: author.login
        }
        mongoose.connect(dbUrl, connectOptions).then(() => model.insertMany(new model(character))).catch((error) => { res.status(500); return; });
        res.status(201);
        res.json(character);
        character.type = 'add';
        amqp.connect(config.rabbitmq)
            .then(connection => {
                return connection.createChannel();
            })
            .then(channel => {
                const queue = 'characters';
                channel.assertQueue(queue);
                channel.sendToQueue(queue , Buffer.from(JSON.stringify(character)));
            }).catch(err => console.log('amqp' , err));
    },

    async updateCharacter(req, res) {
        if (req.body == null) {
            res.status(400).send("Bad request");
            return;
        }
        if (req.body._id == null || req.body.name == null || req.body.rank == null || req.body.age == null) {
            res.status(400).send("Empty or null input");
            return;
        }
        const token = req.body.token;
        if (token == null && token == undefined) {
            res.status(401).send("No login");
            return;
        }
        const check = checkToken(token);
        if (check == 401) {
            res.status(401).send("No login");
            return;
        }
        const author = unpackToken(token);
        const character = await getCharacterById(req.body._id);
        if(character == 500 || character.length == 0)
        {
            res.status(500).send("DB error");
            return;
        }
        if (character[0].author != author.login) {
            res.status(403).send("Not author logined");
            return;
        }
        const update_character = {
            _id: req.body._id,
            name: req.body.name,
            rank: req.body.rank,
            age: req.body.age,
            author: author.login
        }
        await updateCharacterById(update_character);
        res.status(200);
        res.json(update_character);
        update_character.type = 'update';
        amqp.connect(config.rabbitmq)
            .then(connection => {
                return connection.createChannel();
            })
            .then(channel => {
                const queue = 'characters';
                channel.assertQueue(queue);
                channel.sendToQueue(queue , Buffer.from(JSON.stringify(update_character)));
            }).catch(err => console.log('amqp' , err));
    },

    async deleteCharacter(req , res)
    {
        if (req.body == null) {
            res.status(400).send("Bad request");
            return;
        }
        const token = req.body.token;
        if (token == null && token == undefined) {
            res.status(401).send("No login");
            return;
        }
        const check = checkToken(token);
        if (check == 401) {
            res.status(401).send("No login");
            return;
        }
        const author = unpackToken(token);
        var character = await getCharacterById(req.body._id);
        if (character == 500 || character.length == 0) {
            res.status(500).send("Error");
            return;
        }
        if(character[0].author != author.login)
        {
            res.status(403).send("Not author logined");
            return;
        }
        mongoose.connect(dbUrl , connectOptions).then(() => model.find({})).then(() => {return model.remove({_id: req.body._id})}).catch((error)=> {return 500});
        res.status(200).json(character[0]);
        character = character[0];
        const queue_character = {
            _id: character._id,
            name: character.name,
            rank: character.rank,
            age: character.age,
            author: character.author,
            type: 'delete'
        }
        amqp.connect(config.rabbitmq)
            .then(connection => {
                return connection.createChannel();
            })
            .then(channel => {
                const queue = 'characters';
                channel.assertQueue(queue);
                channel.sendToQueue(queue , Buffer.from(JSON.stringify(queue_character)));
            }).catch(err => console.log('amqp' , err));
    }
}