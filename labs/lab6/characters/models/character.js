const { Schema } = require("mongoose");
const { ObjectID } = require('mongodb');

const Character = new Schema({
    _id: ObjectID,
    name: String,//character name
    rank: Number,//character rank
    age: Number,//age of character
    author: String,//user-name of creator
})

module.exports = Character;