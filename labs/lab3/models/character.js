
/**
 * @typedef Character
 * @property {integer} id
 * @property {string} name.required - name of Heroic spirit
 * @property {string} role.required - Heroic spirit class
 * @property {integer} age - age of Heroic spirit while alive
 * @property {string} releaseDate.required - date of character release
 * @property {string} picUrl - URL adress of Heroic spirit picture
 * @property {number} rank.required - Rarity rank of Heroic spirit
 */
class Character{

    constructor(id, name, role, age, rank, releaseDate, picUrl)
    {
        this.id=id,
        this.name=name, 
        this.role=role, 
        this.age=age,
        this.rank=rank, 
        this.releaseDate=releaseDate,
        this.picUrl=picUrl;
    }
};
module.exports = Character;