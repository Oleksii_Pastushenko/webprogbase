const fs = require('fs');
const path = require('path');
const url = require('url');
const mustache = require("mustache");
module.exports =
{
    getPage(req,res)
    {
        const userTemplate = fs.readFileSync(__dirname+"./../views/index.mst").toString();    
        const footPartialTemplate = fs.readFileSync(__dirname+"./../views/partials/footer.mst").toString();
        const headPartialTemplate = fs.readFileSync(__dirname+"./../views/partials/head.mst").toString();
        const headerPartialTemplate = fs.readFileSync(__dirname+"./../views/partials/header.mst").toString();
        var dat=
        {
            homes:'active'
        }       
        const heads=mustache.render(headerPartialTemplate,dat);
        dat = 
        {
            name:'Home'
        }
        const nm=mustache.render(headPartialTemplate,dat);
        const data =
        {
            footer:footPartialTemplate,
            header:heads,
            head:nm,
        }
        const resultString = mustache.render(userTemplate,data);
        //console.log(resultString);
        fs.writeFileSync(__dirname+"./../views/rst.html",resultString);
        res.render('rst');
    }
};