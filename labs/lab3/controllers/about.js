const fs = require('fs');
const path = require('path');
const url = require('url');
const mustache = require("mustache");

function readWebDoc(fileName) {
    const filePath = path.join(__dirname, fileName);
    console.log(__dirname);
    return fs.readFileSync();
 };
 
 
module.exports =
{
    getPage(req,res)
    {
        const userTemplate = fs.readFileSync(__dirname+"./../views/about.mst").toString();    
        const footPartialTemplate = fs.readFileSync(__dirname+"./../views/partials/footer.mst").toString();
        const headPartialTemplate = fs.readFileSync(__dirname+"./../views/partials/head.mst").toString();
        const headerPartialTemplate = fs.readFileSync(__dirname+"./../views/partials/header.mst").toString();
        var dat=
        {
            about_a:'active'
        }       
        const heads=mustache.render(headerPartialTemplate,dat);
        dat = 
        {
            name:'About'
        }
        const nm=mustache.render(headPartialTemplate,dat);
        console.log(heads);
        const data =
        {
            footer:footPartialTemplate,
            header:heads,
            head:nm,
        }
        const resultString = mustache.render(userTemplate,data);
        //console.log(resultString);
        fs.writeFileSync(__dirname+"./../views/rst.html",resultString);
        res.render('rst');
    }
};