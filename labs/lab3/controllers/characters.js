const Character = require('../models/character');
const CharacterRepository = require('../repositories/characterRepository');
const characterRepository = new CharacterRepository('./data/characters.json');
const MediaRepository = require('../repositories/mediaRepository');
const mediaRepository = new MediaRepository('./data/media.json');
const mustache = require("mustache");
const fs=require('fs');
const path=require('path');
module.exports = 
{
    getCharacters(req, res) { 
        if (!('pages' in req.query))
        {        
            const footPartialTemplate = fs.readFileSync(__dirname+"./../views/partials/footer.mst").toString();
            const headPartialTemplate = fs.readFileSync(__dirname+"./../views/partials/head.mst").toString();
            const headerPartialTemplate = fs.readFileSync(__dirname+"./../views/partials/header.mst").toString();
            const userPartialTemplate = fs.readFileSync(__dirname+"./../views/partials/character-partials.mst").toString();
            const userTemplate = fs.readFileSync(__dirname+"./../views/charactres.mst").toString();
            const data=characterRepository.getCharacters();
            var dat=
            {
                characters_a:'active'
            }       
            console.log(headerPartialTemplate);
            const heads=mustache.render(headerPartialTemplate,dat);
            console.log(heads);
            dat = 
            {
                name:'Characters'
            }
            const nm=mustache.render(headPartialTemplate,dat);
            const userData = {
                characters:data,
                footer:footPartialTemplate,
                header:heads,
                head:nm,
                result:'Character list'
            };
            const partials = {
                character: userPartialTemplate
            };
            const resultString = mustache.render(userTemplate, userData, partials);
        
            fs.writeFileSync(__dirname+"./../views/rst.html",resultString);
            //res.render('rst');
        }
        else{
            console.log(req.originalUrl)
            var cur_page=1;
            if(('page' in req.query))
            {
                cur_page=Number.parseInt(req.query.page);
            }
            const pages = Number.parseInt(req.query.pages);
            const items = Number.parseInt(req.query.items);
            const name=req.query.name;
            const footPartialTemplate = fs.readFileSync(__dirname+"./../views/partials/footer.mst").toString();
            const headPartialTemplate = fs.readFileSync(__dirname+"./../views/partials/head.mst").toString();
            const headerPartialTemplate = fs.readFileSync(__dirname+"./../views/partials/header.mst").toString();
            const userPartialTemplate = fs.readFileSync(__dirname+"./../views/partials/character-partials.mst").toString();
            const userTemplate = fs.readFileSync(__dirname+"./../views/charactres.mst").toString();
            const data=characterRepository.getCharacters();
            var characters_to_view = new Array();
            for(const i of data)
            {
                if(i.name.includes(name))
                {
                    characters_to_view.push(i);
                }
            }
            
            var e_res='';
            console.log(characters_to_view.length);
            if(characters_to_view.length===0)
            {

                e_res='Nothing found';
                console.log(e_res);
            }
            if(cur_page===0)
            {
                cur_page=1;
                e_res='No such page'
            }
            if(cur_page>pages)
            {
                cur_page=pages;
                e_res='No such page'
            }
            const items_total = characters_to_view.length;       
            const pages_total = Math.ceil(items_total/items);
            const cur_index=items*(cur_page-1);
            var arr=new Array()
            for(var i=cur_index;i<cur_index+items;i++)
            {
                if(i > (items_total - 1))
                {
                    break;
                }
                arr.push(characters_to_view[i]);
            }
            var dat=
            {
                characters_a:'active'
            }       
            const heads=mustache.render(headerPartialTemplate,dat);
            dat = 
            {
                name:'Characters'
            }
            const nm=mustache.render(headPartialTemplate,dat);
            const res='Результат поиска имени:'+name;
            const userData = {
                characters:arr,
                footer:footPartialTemplate,
                header:heads,
                head:nm,
                cur_pages:cur_page,
                total_pages:pages,
                itemss:items,
                pagess:pages,
                names:name,
                result:res,
                error:e_res,
                next:cur_page+1,
                prev:cur_page-1
            };
            const partials = {
                character: userPartialTemplate
            };
            const resultString = mustache.render(userTemplate, userData, partials);
        
            fs.writeFileSync(__dirname+"./../views/rst.html",resultString);
        }
        res.render('rst');
    },
    getCharacterById(req, res) 
    {
        const id = parseInt(req.params.id);       
        const character = characterRepository.getCharacterById(id);                
        if(character !== null)
        {
            res.status(200);
            const userTemplate = fs.readFileSync(__dirname+"./../views/character.mst").toString();
            const footPartialTemplate = fs.readFileSync(__dirname+"./../views/partials/footer.mst").toString();
            const headPartialTemplate = fs.readFileSync(__dirname+"./../views/partials/head.mst").toString();
            const headerPartialTemplate = fs.readFileSync(__dirname+"./../views/partials/header.mst").toString();
            const data={
                url:'./../media/'+character.id,
                id:character.id,
                name:character.name,
                role:character.role,
                age:character.age,
                releaseDate:character.releaseDate,
                footer:footPartialTemplate,
                header:heads,
                head:nm
            };           
            const resultString = mustache.render(userTemplate,data);          
            fs.writeFileSync(__dirname+"./../views/rst.html",resultString);
            res.render('rst.html');
        }
        else
        {
            res.status(404);
            res.json({ });  
        }
    },
    newPage(req,res)
    {
        const footPartialTemplate = fs.readFileSync(__dirname+"./../views/partials/footer.mst").toString();
            const headPartialTemplate = fs.readFileSync(__dirname+"./../views/partials/head.mst").toString();
            const headerPartialTemplate = fs.readFileSync(__dirname+"./../views/partials/header.mst").toString();
            const userTemplate = fs.readFileSync(__dirname+"./../views/new.mst").toString();
            var dat=
            {
                characters_a:'active'
            }       
            const heads=mustache.render(headerPartialTemplate,dat);
            dat = 
            {
                name:'Characters'
            }
            const nm=mustache.render(headPartialTemplate,dat);
            const userData = {
                footer:footPartialTemplate,
                header:heads,
                head:nm,
                id:characterRepository.getLastId()+1
            };
            const resultString = mustache.render(userTemplate, userData);
        
            fs.writeFileSync(__dirname+"./../views/rst.html",resultString);
            res.render('rst');
    },
    createCharacter(req, res) 
    { 
        try
        {
            const character_object = req.body;        
            const new_name = character_object.name;
            const new_role = character_object.role;
            const new_rank = parseInt(character_object.rank);
            const new_age = Number(character_object.age);
            const new_rel_date = character_object.releaseDate;
            const new_pic = "./../media/"+mediaRepository.get_next_id();                  
            const character = new Character(-1,new_name,new_role,new_age,new_rank,new_rel_date,new_pic);
            characterRepository.addCharacter(character);
            mediaRepository.addMedia(req.files.image);
            res.redirect('/api/characters/'+characterRepository.getLastId());
        }
        catch(e)
        {            
            console.log(e);
            res.redirect('/api/characters/'+characterRepository.getLastId());
        }
        
    },
    updateCharacter(req, res) {
        // try
        // {
        //     character_object = req.body;
        //     if(!('id' in character_object))
        //     {
        //         res.status(400);
        //         res.json({ });
        //         return; 
        //     }
        //     const ob_id=parseInt(character_object.id);
        //     if(isNaN(ob_id))
        //     {
        //         res.status(400);
        //         res.json({ });
        //         return; 
        //     }
        //     if(ob_id > characterRepository.getLastId())
        //     {
        //         res.status(400);
        //         res.json({ });
        //         return; 
        //     }
        //     if (!('name' in character_object))
        //     {
        //         res.status(400);
        //         res.json({ });
        //         return;
        //     }           
        //     const new_name = character_object.name;
        //     if (!('role' in character_object))
        //     {
        //         res.status(400);
        //         res.json({ });
        //         return;
        //     }
            
        //     const new_role = character_object.role;
        //     if (!('rank' in character_object))
        //     {
        //         res.status(400);
        //         res.json({ });
        //         return;
        //     }
            
        //     const new_rank = Number(character_object.rank);
        //     if(!Number.isInteger(new_rank))
        //     {
        //         res.status(400);
        //         res.json({ });
        //         return;
        //     }
        //     if((new_rank < 0)||(new_rank > 5))
        //     {
        //         res.status(400);
        //         res.json({ });
        //         return;
        //     }
        //     if (!('releaseDate' in character_object))
        //     {
        //         res.status(400);
        //         res.json({ });
        //         return;
        //     }
        //     const new_rel_date = character_object.releaseDate;
        //     if(isNaN(Date.parse(new_rel_date)))
        //     {
        //         res.status(400);
        //         res.json({ });
        //         return;
        //     }
        //     if (!('picUrl' in character_object))
        //     {
        //         res.status(400);
        //         res.json({ });
        //         return;
        //     }
        //     const new_pic = character_object.picUrl;                    
        //     const character = new Character(ob_id,new_name,new_role,new_rank,new_rel_date,new_pic);
        //     new_id = characterRepository.updateCharacter(character);         
        //     res.status(201);
        //     res.json(character);
        // }
        // catch(e)
        // {            
        //     console.log(e);
        //     res.status(400);
        //     res.json({ });
        // }
    },
    deleteCharacterById(req, res) {
        const id=Number(req.params.id);
        mediaRepository.deleteMedia(id);
        characterRepository.deleteCharacter(id);
        res.redirect("/api/characters");
    } 
};

