
const UserRepository = require('../repositories/userRepository');
const userRepository = new UserRepository('./data/users.json');
const mustache = require("mustache");
module.exports = 
{

    getUsers(req, res) 
    {
        const footPartialTemplate = fs.readFileSync(__dirname+"./../views/partials/footer.mst").toString();
        const headPartialTemplate = fs.readFileSync(__dirname+"./../views/partials/head.mst").toString();
        const headerPartialTemplate = fs.readFileSync(__dirname+"./../views/partials/header.mst").toString();
        const userPartialTemplate = fs.readFileSync(__dirname+"./../views/partials/user-partials.mst").toString();
        const userTemplate = fs.readFileSync(__dirname+"./../views/users.mst").toString();
        const data=userRepository.getUsers();
        var dat=
        {
            users_a:'active'
        }
        const heads=mustache.render(headerPartialTemplate,dat);
        dat = 
        {
            name:'Users'
        }
        const nm=mustache.render(headPartialTemplate,dat);
        const partials = {
            user: userPartialTemplate
        };
        const userData = {
            users:data,
            footer:footPartialTemplate,
            header:heads,
            head:nm
        };
        const resultString = mustache.render(userTemplate,userData, partials);
        fs.writeFileSync(__dirname+"./../views/rst.html",resultString);
        res.render('rst');        
    },
    getUserById(req, res) {
        const id = parseInt(req.params.id);       
        const user = userRepository.getUserById(id);                
        if(user !== null)
        {
            res.status(200);
            const userTemplate = fs.readFileSync(__dirname+"./../views/user.mst").toString();
            const resultString = mustache.render(userTemplate, user);            
            fs.writeFileSync(__dirname+"./../views/rst.html",resultString);
            res.render('rst.html');
        }
        else
        {
            res.status(404);
            res.json({ });  
        }
        // const id = parseInt(req.params.id);        
        // res.set("Content-type", "application/json");
        // const user = userRepository.getUserById(id);
        // if(user !== null)
        // {            
        //     res.status(200);            
        //     res.json(user);
        // }
        // else
        // {            
        //     res.status(404);
        //     res.json({ });                     
        // }
    }, 
};

