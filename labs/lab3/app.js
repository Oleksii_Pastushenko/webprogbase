
const apiRouter = require('./routes/api');
const express = require('express');
const morgan = require('morgan');
const consolidate = require('consolidate');
const path = require('path');
const app = express();
app.engine('html', consolidate.swig);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');


const busboy = require('busboy-body-parser');

const options_busboy = {
   limit: '5mb',
   multi: false,
};

app.use(busboy(options_busboy));

const bodyParser = require('body-parser');
app.use(bodyParser.json({ extended: true }));

app.use(morgan('dev'));
app.use(express.static('public'));
app.use('/api', apiRouter);

const port = 3000

const expressSwaggerGenerator = require('express-swagger-generator');
const expressSwagger = expressSwaggerGenerator(app);
 
const options = {
    swaggerDefinition: {
        info: {
            description: 'JSON HTTP API server',
            title: 'Character server',
            version: '1.0.0',
        },
        host: `localhost:${port}`,
        produces: [ "application/json" ],
    },
    basedir: __dirname,
    files: ['./routes/**/*.js', './models/**/*.js'],
};
expressSwagger(options);


app.use(function(req, res) {  console.log('Any request');  });

app.listen(port, function() { console.log('Server is ready');});










