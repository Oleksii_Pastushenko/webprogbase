const User = require('../models/user');
const JsonStorage = require('../jsonStorage');
 
class UserRepository 
{
 
    constructor(filePath) 
    {
        this.storage = new JsonStorage(filePath);
    }
 
    getUsers() 
    { 
        const jsonObject = this.storage.readItems();
        return jsonObject.items;  
    }
 
    getUserById(id) 
    {
        const items = this.getUsers();
        const new_id = items.findIndex(item => item.id === id);
        
            if (new_id !== -1) 
            {
                const user = new User(items[new_id].id, items[new_id].login, items[new_id].fullname,items[new_id].role,items[new_id].registeredAt,items[new_id].avaUrl,items[new_id].isEnabled);
                

                return user;
            
            }
        return null;
    }

    addUser(userModel)
    {
        const user = new User(-1, userModel.login, userModel.fullname,userModel.role,userModel.registeredAt,userModel.avaUrl,userModel.isEnabled);
        user.id = this.storage.nextId;
        this.storage.incrementNextId();
        const usersObject = this.storage.readItems();
        usersObject.items.push(user);
        this.storage.writeItems(usersObject);



    }
    updateUser(userModel)
    {
        const usersObject = this.storage.readItems();
        const index_to_update = usersObject.items.findIndex(x => x.id === userModel.id);
        usersObject.items[index_to_update] = userModel;
        this.storage.writeItems(usersObject);
    }
    deleteUser(id)
    {
        
        const usersObject = this.storage.readItems();
        const index_to_delete = usersObject.items.findIndex(x => x.id === id);
        usersObject.items.splice(index_to_delete,1);
        this.storage.writeItems(usersObject);
    }
};
 
module.exports = UserRepository;
