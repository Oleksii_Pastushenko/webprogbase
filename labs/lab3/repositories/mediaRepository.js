const Media = require('../models/media');
const JsonStorage = require('../jsonStorage');

class MediaRepository 
{
 
    constructor(filePath) 
    {
        this.storage = new JsonStorage(filePath);
    }

    getMedias() 
    { 
        const jsonObject = this.storage.readItems();
        return jsonObject.items;  
    }

    getMediaById(id) 
    {
        const items = this.getMedias();
        const new_id = items.findIndex(item => item.id === id);
        
            if (new_id !== -1) 
            {
                const media_file = new Media(items[new_id].id,items[new_id].path);                                     
                return media_file;            
            }
        return null;
    }

    deleteMedia(id)
    {
        const mediaObject = this.storage.readItems();
        const index_to_delete = mediaObject.items.findIndex(x => x.id === id);
        const deleted=this.getMediaById(id);
        fs.unlinkSync(deleted.path);
        mediaObject.items.splice(index_to_delete,1);
        this.storage.writeItems(mediaObject);
    }

    addMedia(file)
    {              
        const id = this.storage.nextId;
        const mime = require('mime-types');
        const ext=mime.extension(file.mimetype);
        const path="C:\\Users\\Admin\\Documents\\webprogbase\\webprogbase\\labs\\lab3\\data\\media\\"+id+'.'+ext;
        const media_file = new Media(id,path);
        this.storage.incrementNextId();
        const mediasObject = this.storage.readItems();
        mediasObject.items.push(media_file);
        this.storage.writeItems(mediasObject);
        fs.writeFileSync("C:\\Users\\Admin\\Documents\\webprogbase\\webprogbase\\labs\\lab3\\data\\media\\"+id+'.'+ext,file.data);
        return media_file.id;
    }

    get_next_id()
    {
        const jsonObject = this.storage.readItems();
        return jsonObject.nextId;  
    }
}  
module.exports = MediaRepository;