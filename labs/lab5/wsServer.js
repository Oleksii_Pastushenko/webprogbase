let usersController = require("./usersController");
const MessageTypes = require('./messageTypes');

const jwt = require('jsonwebtoken');
const jwtSecret = require('./config').jwtSecret;

const WebSocketServer = require('ws').Server;

class AppMessage {
    constructor(token, type = "undefined", payload = null) {
        this.token = token;
        this.type = type;
        this.payload = payload;
    }
}
class User{
    constructor(connection, login=null){
        this.connection=connection;
        this.login=login;
    }
}
class ChatServer{

    constructor(server){
        this.connections = [];
        
        usersController.wsSend = this.sendMessage;
        
        this.wsServer = new WebSocketServer({ server });
        this.wsServer.on("connection", connection => {
            this.addConnection(connection);
            console.log("(+) new connection. total connections:", this.connections.length);
            connection.on("message", message => this.perform(message, connection));
            connection.on("close", () => this.onClose(connection) );
          });
    }
    addConnection(connection){
        this.connections.push(connection);
    }
    perform(message, connection){
        const msg = JSON.parse(message);
        console.log(msg.type);
        try{
            switch(msg.type){
                case MessageTypes.CLIENT_GET_TOKEN:{
                    const user = {
                        login: msg.payload.login,
                        telegramLogin: msg.payload.telegramLogin,
                        wsConnection: connection
                    }
                    const content = usersController.addUser(user);
                    console.log(content);
                    if (content.isLoginNew){
                            const token = jwt.sign(user.login, jwtSecret);
                            const appmsg = new AppMessage(token, MessageTypes.SERVER_TOKEN, token);
                            this.sendMessage(user.wsConnection, appmsg);
                    } else
                    if (!content.isTelegramLoginCorrect){
                        const appmsg = new AppMessage(null, "error", "Error: telegram login isn't correct");
                        this.sendMessage(user.wsConnection, appmsg);
                    } else{
                        this.connections.splice(this.connections.indexOf(connection), 1);
                        const token = jwt.sign(user.login, jwtSecret);
                        const appmsg = new AppMessage(token, MessageTypes.SERVER_TOKEN, token);
                        this.sendMessage(connection, appmsg);
                        if (content.room){
                            const appmsg = new AppMessage(token, MessageTypes.SERVER_CURRENT_ROOM_CHANGED, content.room);
                            this.sendMessage(connection, appmsg);
                        }
                    }
                    break;
                }
                case MessageTypes.CLIENT_GET_ROOMS_LIST:{
                    const user = jwt.verify(msg.token, jwtSecret);
                    const rooms = usersController.getRooms();
                    const appmsg = new AppMessage(null, MessageTypes.SERVER_ROOMS_LIST, rooms.map(room => room.chatroom));
                    this.sendMessage(connection, appmsg);
                    break;
                }
                case MessageTypes.CLIENT_CREATE_ROOM:{
                    const user = jwt.verify(msg.token, jwtSecret);
                    const chatroom = usersController.createRoom(user,msg.payload);
                    const appmsg = new AppMessage(null, MessageTypes.SERVER_ROOM_CREATED, chatroom);
                    usersController.notifyMany(appmsg);
                    break;
                }
                case MessageTypes.CLIENT_JOIN_ROOM:{
                    const user = jwt.verify(msg.token, jwtSecret);
                    const room = usersController.joinRoom(user, msg.payload);
                    if (!room){
                        console.log("No such room");
                    }
                    let appmsg = new AppMessage(null, MessageTypes.SERVER_MEMBER_JOINED, user);
                    usersController.notifyMany(appmsg, room.members);
                    appmsg = new AppMessage(null, MessageTypes.SERVER_CURRENT_ROOM_CHANGED, room.chatroom);
                    usersController.notify(appmsg, user);
                    break;
                }
                case MessageTypes.CLIENT_LEAVE_ROOM:{
                    const user = jwt.verify(msg.token, jwtSecret);
                    const room = usersController.leaveRoom(user, msg.payload);
                    let appmsg = new AppMessage(null, MessageTypes.SERVER_CURRENT_ROOM_CHANGED, null);
                    usersController.notify(appmsg, user);
                    appmsg = new AppMessage(null, MessageTypes.SERVER_MEMBER_LEFT, user);
                    usersController.notifyMany(appmsg, room.members);
                    break;
                }
                case MessageTypes.CLIENT_RENAME_ROOM:{
                    const user = jwt.verify(msg.token, jwtSecret);
                    const content = usersController.renameRoom(user,msg.payload);
                    const appmsg = new AppMessage(null, MessageTypes.SERVER_ROOM_RENAMED, content);
                    usersController.notifyMany(appmsg);
                    break;
                }
                case MessageTypes.CLIENT_GET_MEMBERS_LIST:{
                    const user = jwt.verify(msg.token, jwtSecret);
                    const room = usersController.getRoom({name: msg.payload});
                    const appmsg = new AppMessage(null, MessageTypes.SERVER_MEMBERS_LIST, room.members);
                    this.sendMessage(connection, appmsg);
                    break;
                }
                case MessageTypes.CLIENT_REMOVE_ROOM:{
                    const user = jwt.verify(msg.token, jwtSecret);
                    const room = usersController.removeRoom(user,msg.payload);
                    let appmsg = new AppMessage(null, MessageTypes.SERVER_ROOM_REMOVED, room.chatroom);
                    usersController.notifyMany(appmsg);
                    appmsg = new AppMessage(null, MessageTypes.SERVER_CURRENT_ROOM_CHANGED, null);
                    usersController.notifyMany(appmsg, room.members);
                    break;
                }
                case MessageTypes.CLIENT_POST_MESSAGE:{
                    const user = jwt.verify(msg.token, jwtSecret);
                    const whatAndTo = usersController.postMessage(user, msg.payload);
                    const appmsg = new AppMessage(null, MessageTypes.SERVER_MESSAGE_POSTED, whatAndTo.message);
                    usersController.notifyMany(appmsg, whatAndTo.members);
                    break;
                }
                case MessageTypes.CLIENT_GET_LAST_MESSAGES_LIST:{
                    const user = jwt.verify(msg.token, jwtSecret);
                    const room = usersController.getRoom({name: msg.payload}, {login: user});
                    if (!room) throw Error("You are not in this room.");
                    const appmsg = new AppMessage(null, MessageTypes.SERVER_LAST_MESSAGES_LIST, room.messages);
                    this.sendMessage(connection, appmsg);
                    break;
                }
                case MessageTypes.CLIENT_LOGOUT:
                {
                    this.connections.splice(this.connections.indexOf(connection), 1);
                    usersController.clearWS(connection);
                    break;
                }
            }
        }
        catch(err){
            console.log(err.message);
        }
    }
    onClose(connection){

        this.connections.splice(this.connections.indexOf(connection), 1);
        usersController.clearWS(connection);

    }
    sendMessage(connection, msg){
        connection.send(JSON.stringify(msg));
    }
    checkFunc(arr){
        return (item)=>{
            if (arr.indexOf(item.login)!==-1) return true;
            else return false;
        }
    }
}

module.exports = ChatServer;