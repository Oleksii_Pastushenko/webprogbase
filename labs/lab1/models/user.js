class User {
    constructor(id, login, fullname, role, registeredAt, avaUrl, isEnabled) {
        this.id = id;  // number
        this.login = login;  // string
        this.fullname = fullname;  // string
        this.role=role, // int
        this.registeredAt=registeredAt, // date ISO 8601
        this.avaUrl=avaUrl, // string
        this.isEnabled=isEnabled; // bool
    }
 };
 
 module.exports = User;