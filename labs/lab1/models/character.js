class Character{
    constructor(id, name, role, age, rank, releaseDate, picUrl)
    {
        this.id=id, // int
        this.name=name, //string
        this.role=role, //string
        this.age=age, //int
        this.rank=rank, //int
        this.releaseDate=releaseDate, //date ISO 8601
        this.picUrl=picUrl; // string
    }
};
module.exports = Character;