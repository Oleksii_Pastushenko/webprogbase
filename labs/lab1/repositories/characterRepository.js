const Character = require('../models/character');
const JsonStorage = require('../jsonStorage');
 
class CharacterRepository 
{
 
    constructor(filePath) 
    {
        this.storage = new JsonStorage(filePath);
    }
 
    getCharacters() 
    { 
        const jsonObject = this.storage.readItems();
        return jsonObject.items;  
    }
 
    getCharacterById(id) 
    {
        const items = this.getCharacters();
        const new_id = items.findIndex(item => item.id === id);      
        if (new_id !== -1) 
        {
            const character = new Character(items[new_id].id, items[new_id].name, items[new_id].role,items[new_id].age,items[new_id].rank,items[new_id].releaseDate,items[new_id].picUrl);
            return character;
        }
        return null;
    }

    addCharacter(characterModel)
    {
        const character = new Character(-1, characterModel.name, characterModel.role,characterModel.age,characterModel.rank,characterModel.releaseDate,characterModel.picUrl);
        character.id = this.storage.nextId;
        this.storage.incrementNextId();
        const charactersObject = this.storage.readItems();
        charactersObject.items.push(character);
        this.storage.writeItems(charactersObject);
    }
    updateCharacter(characterModel)
    {
        const charactersObject = this.storage.readItems();
        const index_to_update = charactersObject.items.findIndex(x => x.id === characterModel.id);
        charactersObject.items[index_to_update] = characterModel;
        this.storage.writeItems(charactersObject);
    }
    deleteCharacter(id)
    {
        const charactersObject = this.storage.readItems();
        const index_to_delete = charactersObject.items.findIndex(x => x.id === id);
        charactersObject.items.splice(index_to_delete,1);
        this.storage.writeItems(charactersObject);
    }
};
module.exports = CharacterRepository;