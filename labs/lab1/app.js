const UserRepository = require('./repositories/userRepository');
const CharacterRepository = require('./repositories/characterRepository');
const Character = require('./models/character');
const User = require('./models/user');
const readline = require('readline-sync');
const userRepository = new UserRepository("data/users.json");
const characterRepository = new CharacterRepository("data/characters.json");


while (true) {
    const input = readline.question("Enter command:");
    console.log(input);
    const parsed= input.split("/");
    if(parsed[0]=="get")
    {        
        if(parsed[1]=="users")
        {
            if(parsed[2]==null)
            {
                console.log(userRepository.getUsers());
            }
            else
            {
                if(userRepository.getUserById(parseInt(parsed[2]))!==null)
                {
                    console.log(userRepository.getUserById(parseInt(parsed[2])));
                }
                else
                {
                    console.log("Such user id not exist")
                }
            }
        }
        else if(parsed[1]=="characters")
        {
            if(parsed[2]==null)
            {
                console.log(characterRepository.getCharacters());
            }
            else
            {
                if(characterRepository.getCharacterById(parseInt(parsed[2]))!==null)
                {
                    console.log(characterRepository.getCharacterById(parseInt(parsed[2])));
                }
                else
                {
                    console.log("Such character id not exist")
                }
            }
        }
        else
        {
            console.log("Command error");
        }
    }
    else if(parsed[0]=="delete")
    {  
        if(parsed[1]=="characters")
        {
            const id=Number(parsed[2]);
            if(!isNaN(id))
            {
                characterRepository.deleteCharacter(id);
            }
            else
            {
                console.log("Enter right id");
            }
        }
        else
        {
            console.log("Command error");
        }
    }
    else if(parsed[0]=="update")
    {  
        const id=Number(parsed[2]);
        if(!isNaN(id))
        {
        
            const newName = readline.question("Enter name:");
            var age;
            const role=readline.question("Enter role:");
            var rank;
            var releaseDate;
            var picUrl=readline.question("Enter pic url:");        
            while(true)
            {
                age=Number(readline.question("Enter new age:"));
                if(!isNaN(age))
                {
                    break;
                }
                console.log("Age error");
            }
            while(true)
            {
                rank=Number(readline.question("Enter new rank:"));
                if(!isNaN(rank) && rank > 0 && rank <=5)
                {
                    break;
                }   
                console.log("Entered not a number, rank must be > 0 add < 6");
            }
            while(true)
            {
            
                releaseDate=readline.question("Enter new date:");
                const check_date=Date.parse(releaseDate);
                if(!isNaN(check_date))
                {
                    break;
                }
                console.log("Enter date in right format YYYY-MM-DD");
            }
            const newChar=new Character(id,newName,role,age,rank,releaseDate,picUrl);
            characterRepository.updateCharacter(newChar);
        }
        else
        {
            console.log("Enter right id");
        }
    }
    else if(parsed[0]=="post")
    {  
        if(parsed[1]=="characters")
        {
            const newName = readline.question("Enter name:");
            var age;
            const role=readline.question("Enter role:");
            var rank;
            var releaseDate;
            var picUrl=readline.question("Enter pic url:");        
            while(true)
            {
                age=Number(readline.question("Enter new age:"));
                if(!isNaN(age))
                {
                    break;
                }
                console.log("Age error");
            }
            while(true)
            {
                rank=Number(readline.question("Enter new rank:"));
                if(!isNaN(rank) && rank > 0 && rank <=5)
                {
                    break;
                }   
                console.log("Entered not a number, rank must be > 0 add < 6");
            }
            while(true)
            {
            
                releaseDate=readline.question("Enter new date:");
                const check_date=Date.parse(releaseDate);
                if(!isNaN(check_date))
                {
                    break;
                }
                console.log("Enter date in right format YYYY-MM-DD");
            }
            const newChar=new Character(0,newName,role,age,rank,releaseDate,picUrl);
            characterRepository.addCharacter(newChar);
        }
        else
        {
            console.log("Command error");
        }
    }
    else
    {
        console.log("Command error");
    }
    
    // TODO
}
