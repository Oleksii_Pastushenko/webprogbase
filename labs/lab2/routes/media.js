const router = require('express').Router();

const mediaController = require('../controllers/media');

router

/**
 * Post image to server
 * @route POST /api/media
 * @group Media - upload and get images
 * @consumes multipart/form-data
 * @param {file} image.formData.required - uploaded image
 * @returns {integer} 200 - added image id
 * @returns {Error} 400 - invalid request body
 */

    .post("/", mediaController.createMedia)

/**
 * Get an image by id
 * @route GET /api/media/{id}
 * @group Media - upload and get images
 * @param {integer} id.path.required - id of the Media - eg: 1
 * @returns {file} 200 - image
 * @returns {Error} 404 - Media not found
 */

    .get('/:id(\\d+)', mediaController.getMediaById);
    
    

module.exports = router;
