const router = require('express').Router();
const characterController = require('../controllers/characters');
router



    /**
 * Get a page with a number of Characters
 * @route GET /api/Characters
 * @group Characters - Character operations
 * @param {integer} page.query.required - page number
 * @param {integer} per_page.query.required - items per page
 * @returns {Array.<Character>} 200 - a page with Characters
 * @returns {Error} 404 - page doesn't exist
 * @returns {Error} 400 - invalid query
 */

    .get("/", characterController.getCharacters)

    /**
 * Get Character model by id
 * @route GET /api/Characters/{id}
 * @group Characters - Character operations
 * @param {integer} id.path.required - id of the req Character - eg: 1
 * @returns {Character.model} 200 - Character object
 * @returns {Error} 404 - Character not found
 */

    .get('/:id(\\d+)', characterController.getCharacterById)

    /**
 * Post a new Character
 * @route POST /api/Characters
 * @group Characters - Character operations
 * @param {Character.model} Character.body.required - new Character object
 * @returns {Character.model} 201 - added Character object
 * @returns {Error} 400 - invalid request
 */

    .post("/", characterController.createCharacter)

    /**
 * Update a Character
 * @route PUT /api/Characters
 * @group Characters - Character operations
 * @param {integer} id.path.required - Id of updating character
 * @param {Character.model} Character.body.required - new object
 * @returns {Character.model} 200 - changed Character
 * @returns {Error} 404 - Character not found
 * @returns {Error} 400 - invalid request
 */

    .put("/", characterController.updateCharacter)

    /**
 * Delete a Character
 * @route DELETE /api/Characters/{id}
 * @group Characters - Character operations
 * @param {integer} id.path.required - id of deleting Character - eg: 1
 * @returns {Character.model} 200 - deleted Character
 * @returns {Error} 404 - Character not found
 */

    .delete('/:id(\\d+)', characterController.deleteCharacterById);
   
module.exports = router;