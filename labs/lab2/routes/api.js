const router = require('express').Router();
const userRouter = require('./users');
const characterRouter = require('./characters');
const mediaRouter = require('./media');
router.use('/users', userRouter);
router.use('/characters', characterRouter);
router.use('/media', mediaRouter);
module.exports = router;