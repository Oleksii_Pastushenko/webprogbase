/**
 * @typedef Media
 * @property {integer} id
 * @property {string} path.required - path
 */
class Media
{
    constructor(id, path)
    {
        this.id = id;
        this.path = path;
    }
}
module.exports = Media;