/**
 * @typedef User
 * @property {integer} id
 * @property {string} login.required - login
 * @property {string} fullname - user fullname
 * @property {integer} role.required - Role of user : 1 - admins, 0 - non admins
 * @property {string} registeredAt.required - user reg date
 * @property {string} avaUrl - URL adress of picture
 * @property {boolean} isEnabled.required - profile state: active/non-active
 */
class User {

    constructor(id, login, fullname, role, registeredAt, avaUrl, isEnabled) {
        this.id = id;  // number
        this.login = login;  // string
        this.fullname = fullname;  // string
        this.role=role,
        this.registeredAt=registeredAt,
        this.avaUrl=avaUrl,
        this.isEnabled=isEnabled;
        // TODO: More fields
    }
 };
 
 module.exports = User;