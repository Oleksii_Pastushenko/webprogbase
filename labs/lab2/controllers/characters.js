const Character = require('../models/character');
const CharacterRepository = require('../repositories/characterRepository');
const characterRepository = new CharacterRepository('./data/characters.json');
module.exports = 
{
    getCharacters(req, res) { 
        res.set("Content-type", "application/json");
        if (!('page' in req.query))
        {
            res.status(400);
            res.json({ });
            return;
        }   
        const page = Number.parseInt(req.query.page);
        if(isNaN(page))
        {
            res.status(400);
            res.json({ });
            return;
        }
        if(page <= 0)
        {
            res.status(400);
            res.json({ });
            return;

        }
        if (!('per_page' in req.query))
        {
            res.status(400);
            res.json({ });
            return;
        }    
        const per_page = Number.parseInt(req.query.per_page);
        if(isNaN(per_page))
        {
            res.status(400);
            res.json({ });
            return;
        }
        if(per_page <= 0)
        {
            res.status(400);
            res.json({ });
            return;
        }
        if(per_page > 100)
        {
            res.status(400);
            res.json({ });
            return;
        }      
        const characters = characterRepository.getCharacters();
        const items_total = characters.length;       
        const pages_total = Math.ceil(items_total/per_page);
        if(page > pages_total)
        {
            res.status(404);
            res.json({ });
            return;
        }
        const first_index = per_page * (page-1);
        var page_characters = new Array();
        for(var i = first_index;i < first_index + per_page; i++)
        {
            if(i > (items_total - 1))
            {
                break;
            }           
            page_characters.push(characters[i]);
        }      
        res.status(200);       
        res.json(page_characters);
    },
    getCharacterById(req, res) 
    {
        const id = parseInt(req.params.id);        
        res.set("Content-type", "application/json");
        const character = characterRepository.getCharacterById(id);
        if(character !== null)
        {
            res.status(200);            
            res.json(character);
        }
        else
        {
            res.status(404);
            res.json({ });  
        }
    },
    createCharacter(req, res) 
    {
        try
        {
            character_object = req.body;
            if (!('name' in character_object))
            {
                res.status(400);
                res.json({});
                return;
            }           
            const new_name = character_object.name;
            if (!('role' in character_object))
            {
                res.status(400);
                res.json({});
                return;
            }
            
            const new_role = character_object.role;
            if (!('rank' in character_object))
            {
                res.status(400);
                res.json({});
                return;
            }
            
            const new_rank = parseInt(character_object.rank);
            if(isNaN(new_rank))
            {
                res.status(400);
                res.json({});
                return;
            }
            if((new_rank < 1)||(new_rank > 5))
            {
                res.status(400);
                res.json({});
                return;
            }
            if (!('age' in character_object))
            {
                res.status(400);
                res.json({});
                return;
            }
            
            const new_age = Number(character_object.age);
            if(!Number.isInteger(new_rank))
            {
                res.status(400);
                res.json({});
                return;
            }
            if (!('releaseDate' in character_object))
            {
                res.status(400);
                res.json({});
                return;
            }
            const new_rel_date = character_object.releaseDate;
            if(isNaN(Date.parse(new_rel_date)))
            {
                res.status(400);
                res.json({});
                return;
            }
            if (!('picUrl' in character_object))
            {
                res.status(400);
                res.json({});
                return;
            }
            const new_pic = character_object.picUrl;                    
            const character = new Character(-1,new_name,new_role,new_age,new_rank,new_rel_date,new_pic);
            new_id = characterRepository.addCharacter(character);
            character.id = new_id;           
            res.status(201);
            res.json(character);
        }
        catch(e)
        {            
            console.log(e);
            res.status(400);
            res.json({});
        }
        
    },
    updateCharacter(req, res) {
        try
        {
            character_object = req.body;
            if(!('id' in character_object))
            {
                res.status(400);
                res.json({ });
                return; 
            }
            const ob_id=parseInt(character_object.id);
            if(isNaN(ob_id))
            {
                res.status(400);
                res.json({ });
                return; 
            }
            if(ob_id > characterRepository.getLastId())
            {
                res.status(400);
                res.json({ });
                return; 
            }
            if (!('name' in character_object))
            {
                res.status(400);
                res.json({ });
                return;
            }           
            const new_name = character_object.name;
            if (!('role' in character_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            
            const new_role = character_object.role;
            if (!('rank' in character_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            
            const new_rank = Number(character_object.rank);
            if(!Number.isInteger(new_rank))
            {
                res.status(400);
                res.json({ });
                return;
            }
            if((new_rank < 0)||(new_rank > 5))
            {
                res.status(400);
                res.json({ });
                return;
            }
            if (!('releaseDate' in character_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            const new_rel_date = character_object.releaseDate;
            if(isNaN(Date.parse(new_rel_date)))
            {
                res.status(400);
                res.json({ });
                return;
            }
            if (!('picUrl' in character_object))
            {
                res.status(400);
                res.json({ });
                return;
            }
            const new_pic = character_object.picUrl;                    
            const character = new Character(ob_id,new_name,new_role,new_rank,new_rel_date,new_pic);
            new_id = characterRepository.updateCharacter(character);         
            res.status(201);
            res.json(character);
        }
        catch(e)
        {            
            console.log(e);
            res.status(400);
            res.json({ });
        }
    },
    deleteCharacterById(req, res) {
        const id = parseInt(req.params.id);        
        res.set("Content-type", "application/json");
        const character = characterRepository.getCharacterById(id);
        if(character !== null)
        {
            res.status(200);            
            characterRepository.deleteCharacter(id)
            res.json(character);
        }
        else
        {            
            res.status(404);
            res.json({ });
        } 
    } 
};

