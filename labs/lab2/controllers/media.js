fs = require('fs');
const MediaRepository = require('../repositories/mediaRepository');
const mediaRepository = new MediaRepository('./data/media.json');
const path = require('path');
module.exports = {
    createMedia(req, res) 
    {
        try
        {
            res.set("Content-type", "application/json");
            const next_id = mediaRepository.get_next_id().toString();
            const mime = require('mime-types');
            const ext=mime.extension(req.files.image.mimetype);
            const filename = next_id + '.'+ext;
            const filepath = __dirname + `.\\..\\data\\media\\${filename}`;
            fs.writeFileSync(`${filepath}`, req.files.image.data,{flag : "w"});
            mediaRepository.addMedia(filepath);
            res.status(201);
            res.json({id:next_id });
        }
        catch(e)
        {
            res.status(400);
            res.json({"name":e.name });
        }   
    }, 
    getMediaById(req, res) 
    {
        const id = parseInt(req.params.id);        
        res.set("Content-type", "application/json");
        const media_file = mediaRepository.getMediaById(id);
        if(media_file !== null)
        {
            res.status(200);
            res.sendFile(path.resolve(media_file.path));
        }
        else
        {            
            res.status(404);
            res.json({ });                      
        }        
    },    
};